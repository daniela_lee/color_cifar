# -*- coding: utf-8 -*-
from __future__ import print_function, division
import os, datetime, time
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# Ignore warnings
import warnings

warnings.filterwarnings("ignore")
import pickle
import scipy.io
import PIL
import utils
from utils import progress_bar
# from utils2 import progress_bar
# import clustering
import torch.nn as nn
import torch.optim as optim
# from model import ColorNet
from colornet import ColorNet
from sklearn.model_selection import StratifiedKFold
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import argparse
from google.colab import drive
import glob
import cv2

# drive.mount('/content/gdrive')


plt.ion()  # interactive mode

parser = argparse.ArgumentParser(description='Color Constancy')
parser.add_argument('--lr', default=1e-2, type=float, help='learning rate')
parser.add_argument('--epochs', '-e', default=10, type=int, help='total de épocas')
parser.add_argument('--interval', '-i', default=3, type=int,
                    help='define de quantas em quantas epocas sera salvo o modelo ')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--seed', type=int, default=1, metavar='S', help='random seed (default: 1)')
parser.add_argument('--model_dir', default='/content/gdrive/My Drive/VISAO/checkpoint', help='directory of the model')
parser.add_argument('--model_name', default='CNN_Gehler1.pth', type=str, help='the model name')
parser.add_argument('--indexes_dir', default='/content/gdrive/My Drive/VISAO/data/Gehler_dataset/train_indexes',
                    type=str, help='the model name')
parser.add_argument('--folder', type=int, default=1, metavar='S', help='random seed (default: 1)')
parser.add_argument('--batch_size', default=100, type=int, help='batch size')

args = parser.parse_args()
torch.manual_seed(args.seed)

device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
aux_save = 0  # var contadora 0..inter
last_epoch = 0


######################################################################

class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list, labels, real_illu, transform=None):
        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
            real_illu (numpy array)
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        self.transform = transform
        self.real_rgb_list = real_illu

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
        original_image = self.img_names_list[idx][:-4]

        img_name = os.path.join(self.root_dir, original_image, self.img_names_list[idx] + '.tif')

        image = io.imread(img_name)
        image = PIL.Image.fromarray(image)
        real_rgb = self.real_rgb_list[idx]
        label = self.labels[idx]
        #         list_prob_class = np.zeros((np.max(labels)+1,), dtype=int) #vetor de probabilidades, que deve ser maximo na posicao idx
        #         list_prob_class[label] = 1


        sample = {'image': image, 'label': label, 'real_rgb': real_rgb}

        if self.transform:
            sample['image'] = self.transform(sample['image'])

        return sample


######################################################################
# Illumination estimation
def find_estimated_illumination(output):
    e = 0
    for i in range(0, len(real_illu)):
        e += output[i] * real_illu[i]
    return e


######################################################################

# Training
def training(epoch):
    print('\nEpoch: %d' % epoch)
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    batch_loss = 0
    for batch_idx, sample_batched in enumerate(trainloader):
        inputs, targets = sample_batched['image'].to(device), sample_batched['label'].to(device)

        optimizer.zero_grad()

        outputs = net(inputs)

        loss = criterion(outputs, targets)

        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        batch_loss = train_loss / (batch_idx + 1)
        if batch_idx % 10 == 0:
            #             print (total,correct,100.*(correct / total))
            print('%4d %4d / %4d loss = %2.4f acc = %2.4f' % (
            epoch, batch_idx, len(train_names) // args.batch_size, loss.item() / args.batch_size,
            100. * (correct / total)))

    acc = 100. * (correct / total)
    print('Saving epoch:', epoch)
    state = {
        'net': net.state_dict(),
        'loss': batch_loss,
        'last_epoch': epoch,
        'aux_save': aux_save,
        'acc': acc
    }
    if not os.path.isdir('checkpoint'):
        os.mkdir('checkpoint')
    torch.save(state, os.path.join(args.model_dir, args.model_name))


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PREPARING DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Preparing data..')

gehler = {'root_dir': '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches',
          'img_names_labels': '/content/gdrive/My Drive/VISAO/data/k_means_results/groundtruth_568_labels.txt',
          'real_illu_dir': '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/groundtruth_568/real_illum_568.mat',
          }

img_name_list, labels = utils.get_names_and_labels(gehler['img_names_labels'])
real_illu = scipy.io.loadmat(gehler['real_illu_dir'])[
    'real_rgb']  # variavel global que contém os centros dos clusters de iluminação
# print (real_illu.shape,real_illu[0])
# print(img_name_list[567], labels[567])
# # average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)

average_color = (0.28085261, 0.2558284, 0.21423263)
std_color = (0.19844685, 0.18925277, 0.18685587)

x = img_name_list
y = labels

data_transform = transforms.Compose([transforms.RandomRotation((-10, 10)), transforms.RandomRotation((-5, 5)),
                                     transforms.RandomRotation(0), transforms.RandomRotation(5),
                                     transforms.RandomRotation(10),
                                     transforms.RandomCrop(227),
                                     transforms.RandomAffine(0, scale=(250, 1000)),
                                     transforms.RandomHorizontalFlip(),
                                     transforms.ToTensor(),
                                     transforms.Normalize(average_color, std_color),
                                     ])
transform_test = transforms.Compose([
    transforms.RandomCrop(227),
    transforms.ToTensor(),
    transforms.Normalize(average_color, std_color),
])

train_names = scipy.io.loadmat(os.path.join(args.indexes_dir, 'train_set' + str(args.folder) + '.mat'))['train_names']
train_labels = \
scipy.io.loadmat(os.path.join(args.indexes_dir, 'train_set' + str(args.folder) + '.mat'))['train_labels'][0]  # [[]]
# print(len(train_names),train_labels,len(train_labels))


# Codigo que gera as patches
# patches = utils.generate_patches('/content/gdrive/My Drive/VISAO/data/Gehler_dataset/input/','colorconst_cnn/data/k_means_results/groundtruth_568_labels.txt',
#                                 '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches')

train_names, train_labels, real_illu = utils.get_names_and_labels_patches(train_names, train_labels, real_illu, 200)

train_dataset = ColorImageDataset(root_dir=gehler['root_dir'], img_names_list=train_names,
                                  labels=train_labels, real_illu=real_illu, transform=data_transform)

trainloader = DataLoader(train_dataset, batch_size=100, shuffle=True, num_workers=4)

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Building model..')

net = ColorNet()
"""negative log likelihood loss"""
criterion = nn.NLLLoss()
# optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)


optimizer = optim.SGD([{'params': net.features[0].parameters(), 'lr': 10 * args.lr},
                       {'params': net.features[4].parameters()},
                       {'params': net.features[8].parameters()},
                       {'params': net.features[10].parameters()},
                       {'params': net.features[12].parameters()},
                       {'params': net.classifier[0].parameters()},
                       {'params': net.classifier[3].parameters(), 'lr': 10 * args.lr},
                       {'params': net.classifier[6].parameters(), 'lr': 10 * args.lr}
                       ], lr=args.lr, momentum=0.9, weight_decay=5e-4)

net = net.to(device)
if device == 'cuda':
    net = torch.nn.DataParallel(net)
    cudnn.benchmark = True

model_file = glob.glob(os.path.join(args.model_dir, args.model_name))

if len(model_file) > 0:

    checkpoint = torch.load(os.path.join(args.model_dir, args.model_name))
    net.load_state_dict(checkpoint['net'])
    last_epoch = checkpoint['last_epoch'] + 1
    print('resuming by loading; epoch->', last_epoch)
    best_acc = checkpoint['acc']
    aux_save = checkpoint['aux_save']
else:
    print('Começando novo treino...')

for epoch in range(last_epoch, args.epochs):
    # print('epoch',epoch)
    start_time = time.time()
    training(epoch)
    elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | time = %2.4f' % (epoch, elapsed_time))




